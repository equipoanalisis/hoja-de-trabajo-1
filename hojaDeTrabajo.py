#!/usr/bin/python

#Jugete
class Jugete(object):
    """ Jugete de Cemaco.
    Attributes:
        nombre: String con el nombre del jugete
        valorUnitario: un double que contiene el precio inical
        marca: String de la marca
        edad: integer de la edad del jugete para el nino
        proveedor: String con el nombre del proveedor
        tipo: boolean -> false = mecanico y true = electronico
        complejidad: int solo si es mecanico varia del 1 a 3 y si es electronico es siempre 1
        valorAdicional: double todos tienen un valor adicional
    """
    def __init__(self, nombre, valorUnitario, marca, edad, proveedor, tipo, complejidad, valorAdicional):
        self.nombre = nombre
        self.valorUnitario = valorUnitario
        self.marca = marca
        self.edad = edad
        self.proveedor = proveedor
        self.tipo = tipo
        self.complejidad = complejidad
        self.valorAdicional = valorAdicional
